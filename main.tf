provider "aws" {
  profile = "test"
  region = "us-east-1"
}

resource "aws_instance" "web_server_1" {
    ami                     = "ami-00874d747dde814fa"
    instance_type           = "t3.micro"
    vpc_security_group_ids  = [aws_security_group.server_sec.id]
    user_data = file("config.sh")
    tags = {
        Name = "Web server-1"
        }
}

resource "aws_instance" "web_server_2" {
    ami                     = "ami-00874d747dde814fa"
    instance_type           = "t3.micro"
    vpc_security_group_ids  = [aws_security_group.server_sec.id]
    user_data = file("config.sh")
    tags = {
        Name = "Web server-2"
        }
}

resource "aws_instance" "web_server_3" {
    ami                     = "ami-00874d747dde814fa"
    instance_type           = "t3.micro"
    vpc_security_group_ids  = [aws_security_group.server_sec.id]
    user_data = file("config.sh")
    tags = {
        Name = "Web server-3"
        }
}

resource "aws_security_group" "server_sec" {
    name        = "WebServer Sec Group"
    description = "For WebServer"

    ingress {
        from_port   = 0
        to_port     = 0
        protocol    ="-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    egress {
        from_port   = 0
        to_port     = 0
        protocol    ="-1"
        cidr_blocks = ["0.0.0.0/0"] 
    }
}